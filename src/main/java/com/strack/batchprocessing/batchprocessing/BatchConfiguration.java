package com.strack.batchprocessing.batchprocessing;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import javax.xml.crypto.Data;

@Configuration
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;


    //writer
    @Bean
    public JdbcBatchItemWriter<Pet> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Pet>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("UPDATE pets SET age = :phoneNumber WHERE id = :id")
                .dataSource(dataSource)
                .build();
    }

    //reader
    @Bean
    @Autowired
    public JdbcCursorItemReader<Pet> reader(DataSource dataSource) {
        return new JdbcCursorItemReaderBuilder<Pet>()
                .name("petItemReader")
                .rowMapper(new PetRowMapper())
                .sql("SELECT * FROM pets")
                .dataSource(dataSource)
                .build();
    }

    //processor
    @Bean
    public PetItemProcessor processor() {
        return new PetItemProcessor();
    }

    @Bean
    public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
        return jobBuilderFactory.get("importUserJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step1)
                .end().build();
    }

    @Bean
    public Step step1(JdbcBatchItemWriter<Pet> writer, JdbcCursorItemReader<Pet> reader) {
        return stepBuilderFactory.get("step1")
                .<Pet, Pet> chunk(10)
                .reader(reader)
                .processor(processor())
                .writer(writer)
                .build();
    }
}

