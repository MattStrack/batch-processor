//package com.strack.batchprocessing.batchprocessing;
//
//import org.springframework.batch.item.ItemReader;
//import org.springframework.boot.autoconfigure.web.client.RestTemplateAutoConfiguration;
//import org.springframework.boot.web.client.RestTemplateBuilder;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Component;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.Arrays;
//import java.util.List;
//
//@Component
//class RESTPetReader implements ItemReader<Pet> {
//    private final String apiUrl;
//    private final RestTemplate restTemplate;
//
//    private int nextPetIndex;
//    private List<Pet> petData;
//
//    public RESTPetReader(String apiUrl, RestTemplate restTemplate) {
//        this.apiUrl = apiUrl;
//        this.restTemplate = restTemplate;
//        nextPetIndex = 0;
//    }
//
//    @Override
//    public Pet read() throws Exception {
//        if (petDataIsNotInitialized()) {
//            petData = fetchPetDataFromAPI();
//        }
//
//        Pet nextPet = null;
//
//        if(nextPetIndex < petData.size()) {
//            nextPet = petData.get(nextPetIndex);
//            nextPetIndex++;
//        }
//        else {
//            nextPetIndex = 0;
//            petData = null;
//        }
//
//        return nextPet;
//    }
//
//    private boolean petDataIsNotInitialized() {
//        return this.petData == null;
//    }
//
//    private List<Pet> fetchPetDataFromAPI() {
//        ResponseEntity<Pet[]> response = restTemplate.getForEntity(apiUrl, Pet[].class);
//        Pet[] petData = response.getBody();
//        return Arrays.asList(petData);
//    }
//
//}
