package com.strack.batchprocessing.batchprocessing;

import java.util.Random;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.batch.item.ItemProcessor;

public class PetItemProcessor implements ItemProcessor<Pet, Pet> {

    private static final Logger log = LoggerFactory.getLogger(PetItemProcessor.class);
    Random random = new Random();

    @Override
    public Pet process(final Pet pet) throws Exception {
        final String age = generateRandomAge();
        pet.setAge(age);
        return pet;
    }


    //initially used to generate a random phone number but is now no longer needed
    public String generateRandomPhoneNumber() {
        String number = "(";
        number += random.nextInt(9) + 1;
        number += random.nextInt(9) + 1;
        number += random.nextInt(9) + 1 + ")";
        number += random.nextInt(9) + 1;
        number += random.nextInt(9) + 1;
        number += random.nextInt(9) + 1 + "-";
        number += random.nextInt(9) + 1;
        number += random.nextInt(9) + 1;
        number += random.nextInt(9) + 1;
        number += random.nextInt(9) + 1;
        return number;
    }

    public String generateRandomAge() {
        String age = (random.nextInt(15) + 1) + " years";
        return age;
    }
}
