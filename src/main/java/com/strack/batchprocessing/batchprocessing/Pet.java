package com.strack.batchprocessing.batchprocessing;

import lombok.*;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@Data @NoArgsConstructor @AllArgsConstructor
public class Pet {
    private Integer id;
    private String name;
    private String species;
    private String owner;
    private String phoneNumber;
    private String age;

    @Override
    public String toString() {
        return "name: " + name + ", species: " + species + ", owner: " + owner + ", phoneNumber: " + phoneNumber;
    }


}
