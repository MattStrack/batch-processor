package com.strack.batchprocessing.batchprocessing;

import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PetRowMapper implements RowMapper<Pet> {

    @Override
    public Pet mapRow(ResultSet rs, int rowNum) throws SQLException {
        Pet pet = new Pet();
        pet.setId(rs.getInt("id"));
        pet.setName(rs.getString("name"));
        pet.setOwner(rs.getString("owner"));
        pet.setSpecies(rs.getString("species"));
        pet.setPhoneNumber(rs.getString("phone_number"));
        //pet.setAge(rs.getString("age"));
        return pet;
    }

}
